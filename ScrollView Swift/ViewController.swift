//
//  ViewController.swift
//  ScrollView Swift
//
//  Created by Garrick McMickell on 9/5/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Default view/Blue
        let scrollview = UIScrollView(frame:self.view.bounds)
        scrollview.backgroundColor = UIColor.blueColor()
        self.view.addSubview(scrollview)
        scrollview.pagingEnabled = true
        scrollview.contentSize = CGSizeMake(self.view.frame.width * 3, self.view.frame.height * 3)
        
        //Red
        let redView = UIView(frame: CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height))
        redView.backgroundColor = UIColor.redColor()
        scrollview.addSubview(redView)
        
        //Yellow
        let yellowView = UIView(frame: CGRectMake(self.view.frame.size.width * 2, 0, self.view.frame.size.width, self.view.frame.size.height))
        yellowView.backgroundColor = UIColor.yellowColor()
        scrollview.addSubview(yellowView)
        
        //Green
        let greenView = UIView(frame: CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height))
        greenView.backgroundColor = UIColor.greenColor()
        scrollview.addSubview(greenView)
        
        //Purple
        let purpleView = UIView(frame: CGRectMake(self.view.frame.size.width, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height))
        purpleView.backgroundColor = UIColor.purpleColor()
        scrollview.addSubview(purpleView)
        
        //Orange
        let orangeView = UIView(frame: CGRectMake(self.view.frame.size.width * 2, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height))
        orangeView.backgroundColor = UIColor.orangeColor()
        scrollview.addSubview(orangeView)
        
        //White
        let whiteView = UIView(frame: CGRectMake(0, self.view.frame.size.height * 2, self.view.frame.size.width, self.view.frame.size.height))
        whiteView.backgroundColor = UIColor.whiteColor()
        scrollview.addSubview(whiteView)
        
        //Gray
        let grayView = UIView(frame: CGRectMake(self.view.frame.size.width, self.view.frame.size.height * 2, self.view.frame.size.width, self.view.frame.size.height))
        grayView.backgroundColor = UIColor.grayColor()
        scrollview.addSubview(grayView)
        
        //Black
        let blackView = UIView(frame: CGRectMake(self.view.frame.size.width * 2, self.view.frame.size.height * 2, self.view.frame.size.width, self.view.frame.size.height))
        blackView.backgroundColor = UIColor.blackColor()
        scrollview.addSubview(blackView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

